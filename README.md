# Ruby
Ruby es un lenguaje de alto nivel, interpretado y orientado a objetos.

## Ruby y Tipos de Datos

Un tipo de dato es un conjunto de valores. Estos son los tipos de datos más usados en Ruby:

- Strings
- Fixnum 
- Booleans
- Arrays
- Hashes
- Symbols

#### String

Los strings o cadenas de texto son secuencias de caracteres. La sintaxis para definirlos es utilizando comillas simples o comillas dobles.

Cualquier caracter puede ser parte de un string, desde números como '67', hasta caracteres como '!' '@' '^` etc.

```ruby
"Hello World!"
'Ruby y Python'
"hello/$!^"
"1024"
```

#### Fixnum - Integer y Float

Existen dentro de los datos numéricos (Fixnum) muchas categorías, las más comunes son Integer y Float.

Tenemos los números enteros (Integer) y los números con decimales (Float).

Algunos enteros son:

```ruby
100
-500
9999999
0
```
Y algunos ejemplos de flotantes son:

```ruby
73.244
0.0005
-370.467
0.0
```

#### Boolean

El tipo de dato booleano es aquel que solamente puede tomar dos posibles valores. En Ruby estos dos valores son representados por `true` y `false (verdadero y falso).

```ruby
x = false
#=> false
8 > 3
#=> false
100 == 100
#=> true
y = true
#=> true
```

#### Array

Es una lista de datos ordenados numéricamente. Un arreglo puede contener strings, integers, hashes, objetos y otros arreglos.

```ruby
["Carlos", 11, true, "names"]
[6, 3, 10, 100]
[{"nombre" => "carlos"}, [24, 56], "colonia", "ciudad"]
```

#### Hash

Un Hash es una colección de pares `'clave - valor' o 'key - value'` como "nombre" => "sandra". 

```ruby
months = {"3" => "March", "4" => "April"}
estudiante = { "first_name" => "Marcos", "last_name" => "Herran", "age" => 35 }
letras = {:c => 3, :d => 4, :e => 5}
frutas = {manzana: 2, coco: 4, pera: 1}
```

#### Symbol

Un símbolo es una palabra que empieza o acaba con el signo `:`. El símbolo es similar al String, pero a [nivel memoria](http://stackoverflow.com/questions/255078/whats-the-difference-between-a-string-and-a-symbol-in-ruby) se guarda de manera distinta.


```ruby
:name
light = :on
reservacion = :check
``` 

También es posible convertir un string a símbolo con el método `.to_sym`.

```Ruby
c = 'cat'.to_sym
#=> :cat
p = 'dog'.to_sym
#=> :dog
```

Principalmente se utilizan los símbolos como las claves (keys) de un hash. A continuación un ejemplo de como los símbolos son usados como `keys` en hashes:

```ruby
h = {:nickname => 'Herdez', :first_name => 'Kury', :last_name => 'Merlot'}
```
Los símbolos muchas veces usan el signo `:` al final. Esto se usa solamente para ahorrar sintaxis o caracteres, de esta manera se evita usar el hash rocket `=>`.

```ruby
h = {nickname: 'Herdez',  first_name: 'Kury', last_name: 'Merlot'}
```

## Variables y Constantes

Las variables y constantes en Ruby son espacios de memoria reservados que guardan los valores. Las variables como su nombre lo indica pueden variar a lo largo de la ejecución del programa. 


### Definiendo una variable

Ruby reconoce cualquier palabra con las siguientes características como variables:

- Deben empezar con una letra minúscula o un guión bajo `_`.
- Deben estar formadas por letras, números y/o guiones bajos.
- Por convención se estableció que deben ser llamadas de manera tal que definan su funcionalidad, escritas en minúsculas y separadas por `_` .

```ruby
first_name = "Marcos" 
flag3 = true
num_50 = 153254789
```

### Definiendo una constante

Una constante debe ser nombrada con todas las letras en mayúsculas o al menos con la primera letra en mayúscula. El valor de una constante debe permanecer sin cambios durante la ejecución de un programa, es decir no es conveniente modificarla.

```ruby
CONSTANT = 50
#=> 50
CONSTANT = 7
#=> (irb):15: warning: previous definition of CONSTANT was here
#=> 7
```

# Ejercicio - Vamos Definiendo variables en Ruby

```Ruby
#Define tres variable tipo entero.

#Define tres variables tipo flotante.

#Define tres variables tipo boolean.

#Define una variable y asígnale un arreglo que tenga valores tipo string y hash.

#Define una variable y asígnale un hash con tres claves (keys) de tipo symbol, dos valores tipo arreglo (array) y un valor tipo hash con 3 elementos del tipo de dato que decidas.

```